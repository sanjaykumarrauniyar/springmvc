<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/WEB-INF/jsps/header.jsp" />
	<form:form action="${pageContext.request.contextPath}/uploadFile"
		method="post" enctype="multipart/form-data"
		commandName="classStudents">
		<table>
			<tr>
				<td>CLass Name</td>
				<td><form:select path="name">
						<form:options items="${classRoomList}" itemValue="name" itemLabel="name" />
					</form:select></td>
			</tr>
			<tr>
				<td>Students File</td>
				<td><input type="file" name="fileUpload" size="50" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Upload" /></td>
			</tr>
		</table>

	</form:form>
</body>
</html>