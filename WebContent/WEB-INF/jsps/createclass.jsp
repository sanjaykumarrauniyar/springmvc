<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
	<jsp:include page="/WEB-INF/jsps/header.jsp" />
	<form:form method="POST"
		action="${pageContext.request.contextPath}/createclasses"
		commandName="classStudent">
		<table>
			<tr>
				<td>Class Name</td>
				<td><form:input type="text" path="name" id="name" /></td>
			</tr>
			<tr>
				<td>Room No</td>
				<td><form:input type="text" path="roomNo" id="roomNo" /></td>
			</tr>

			<tr>
				<td colspan="3"><c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update" />
						</c:when>
						<c:otherwise>
							<input type="submit" value="save" />
						</c:otherwise>
					</c:choose></td>
			</tr>
		</table>
	</form:form>
</body>
</html>