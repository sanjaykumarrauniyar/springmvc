<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/WEB-INF/jsps/header.jsp" />
	<form:form method="POST" modelAttribute="student">
		<table>
			<tr>
				<td>Student Name</td>
				<td><form:input type="text" path="name"
						value="${vars.student.name}" /></td>
			</tr>


			<tr>
				<td>Address</td>
				<td><form:input type="text" path="address"
						value="${vars.student.address}" /></td>
			</tr>

			<tr>
				<td>Classroom</td>
				<td><form:select path="classroom">
						<c:forEach items="${vars.classrooms}" var="classroom">
							<option
 								<c:if test="${classroom.name eq vars.student.classroom.name}">selected="selected"</c:if>
								value="${classroom.name}">${classroom.name}</option>
						</c:forEach>
					</form:select></td>
			</tr>
			


			<tr>
				<td><input type="submit" value="Update"></td>
				<td></td>
			</tr>

		</table>


	</form:form>

</body>
</html>