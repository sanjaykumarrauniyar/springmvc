<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<jsp:include page="/WEB-INF/jsps/header.jsp" /> 
	
	<img src="${pageContext.request.contextPath}/static/images/logo.jpg" />
	
	<h2>List of Class</h2>
	<table>
		<tr>
			<td>Name</td>
			<td>Room No</td>
			<td></td>
			<td></td>
		</tr>
		<c:forEach items="${classes}" var="classStudent">
			<tr>
				<td>${classStudent.name}</td>
				<td>${classStudent.roomNo}</td>
				<td><a href="<c:url value='/edit-${classStudent.id}-classroom' />">Edit</a></td>
				<td><a href="<c:url value='/delete-${classStudent.id}-classroom' />">Delete</a></td>
				<td><a href="<c:url value='/view-${classStudent.id}-classroom' />">View</a></td>
			</tr>
		</c:forEach>
	</table>
	




</body>
</html>