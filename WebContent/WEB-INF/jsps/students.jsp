<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<jsp:include page="/WEB-INF/jsps/header.jsp" /> 
	
	${classname}
	<h2>List of Students</h2>
	<table>
		<tr>
			<td>Name</td>
			<td>Address</td>
			<td>Classroom</td>
			<td></td>
			<td></td>
		</tr>
		<c:forEach items="${students}" var="student">
			<tr>
				<td>${student.name}</td>
				<td>${student.address}</td>
				<td>${student.classroom.name}</td>
				<td><a href="<c:url value='/edit-${student.id}-student' />">Edit</a></td>
				<td><a href="<c:url value='/delete-${student.id}-student' />">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	




</body>
</html>