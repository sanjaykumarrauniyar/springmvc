package com.springmvclearn.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.service.ClassroomService;

@Controller
public class ClassRoomController {

	@Autowired
	private ClassroomService classsroomService;

	public void setService(ClassroomService classservices) {
		classsroomService = classservices;
	}

	@RequestMapping("/createclass")
	public String showCreateClassroomPage(Model model) {
		//model.addAttribute("edit", false);
		model.addAttribute("classStudent",new Classroom());
		return "createclass";
	}

	@RequestMapping("/createclasses")
	public String saveClassroom(Classroom classes,Model model) {
		classsroomService.saveClassroom(classes);
		return "redirect:/listClasses";
	}

	// @RequestMapping(value = { "/","/listClasses" }, method =
	// RequestMethod.GET)
	// public String listClasses(ModelMap model) {
	// List<ClassStudent> classes = classservice.getAllClassStudent();
	// model.addAttribute("classes", classes);
	// return "classCreatedList";
	// }

	@RequestMapping(value = { "/edit-{classroomID}-classroom" }, method = RequestMethod.GET)
	public String editStudent(@PathVariable int classroomID, ModelMap model) {
		Classroom classroom = classsroomService.getSpecificClassroom(classroomID);
		List<Student> students = classroom.getStudents();
		System.out.println(students.size());
		for(Student student : students){
			System.out.println(student.getName());
		}
		model.addAttribute("classStudent", classroom);
		return "editclassStudent";
	}

	@RequestMapping(value = { "/edit-{classroomID}-classroom" }, method = RequestMethod.POST)
	public String updateClassroom(Classroom classroom, @PathVariable int classroomID,Model model) {
		classsroomService.updateClassroom(classroom,classroomID);
		return "redirect:/listClasses";
	}
	
	@RequestMapping(value = { "/delete-{classroomID}-classroom" }, method = RequestMethod.GET)
	public String deleteClassroom(@PathVariable String classroomID,Model model) {
		 classsroomService.deleteClassroomById(classroomID);
		return "redirect:/listClasses";
	}
	
	
	@RequestMapping(value = { "/view-{classroomID}-classroom" }, method = RequestMethod.GET)
	public String viewClassStudent(@PathVariable int classroomID, ModelMap model) {
		Classroom classroom = classsroomService.getSpecificClassroom(classroomID);
		model.addAttribute("classname", "classroom\t"+ classroom.getName());
		List<Student> students = classroom.getStudents(); // not good practice
		model.addAttribute("students", students);
		return "students";
	}
	
	
	


}
