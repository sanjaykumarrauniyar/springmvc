package com.springmvclearn.web.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.service.ClassroomService;
import com.springmvclearn.web.service.ClassroomStudentsService;
import com.springmvclearn.web.service.StudentsService;
import com.springmvclearn.web.util.CSVReaderUtil;

@Controller
public class ClassRoomStudentsController {

	@Autowired
	private ClassroomService classroomService;
	
	@Autowired
	private StudentsService studentsService;
	
	@Autowired
	private ClassroomStudentsService classroomStudentsService;
	
	private String saveDirectory = "E:\\Test\\";

	@RequestMapping(method = RequestMethod.POST, value = "uploadFile")
	public String handleFileUpload(HttpServletRequest request, @RequestParam CommonsMultipartFile[] fileUpload,
			@ModelAttribute("classStudents")  Classroom classroom) throws Exception {


		if (fileUpload != null && fileUpload.length > 0) {
			for (CommonsMultipartFile aFile : fileUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
				}
			}
		}

		List<Student> students = CSVReaderUtil.readStudentCSVFile();
		
		classroomStudentsService.saveStudentToClassroom(classroom.getName(), students);

		return "sucess";

	}

	
	@RequestMapping(value = "createCLassStudents")
	public String getclassStudentUploadPage(Map<String, Classroom> model) {
		model.put("classStudents", new Classroom());
		return "classStudentFileUpload";
	}

	@ModelAttribute("classRoomList")
	public List<Classroom> getClassroomList() {
		List<Classroom> classrooms = classroomService.getAllClassStudent();
		return classrooms;
	}

}
