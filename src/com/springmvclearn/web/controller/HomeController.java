package com.springmvclearn.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.service.ClassroomService;

@Controller
public class HomeController {
	
	@Autowired
	private ClassroomService classservice;
	
	@RequestMapping(value = { "/","/listClasses" }, method = RequestMethod.GET)
	public String showHome(Model model) {
		List<Classroom> classes = classservice.getAllClassStudent();
		model.addAttribute("classes", classes);
		return "home";
	}

}
