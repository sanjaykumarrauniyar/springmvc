package com.springmvclearn.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.Photo;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.service.ClassroomService;
import com.springmvclearn.web.service.StudentsService;

@Controller
public class StudentsController {

	@Autowired
	private StudentsService studentsService;

	@Autowired
	private ClassroomService classroomService;

	@RequestMapping(value = { "/liststudents" }, method = RequestMethod.GET)
	public String showStudentList(Model model) {
		List<Student> students = studentsService.getAllStudent();
		model.addAttribute("students", students);
		return "students";
	}

	@RequestMapping(value = { "/delete-{studentID}-student" }, method = RequestMethod.GET)
	public String deleteClassroom(@PathVariable String studentID, Model model) {
		studentsService.deleteStudentById(studentID);
		return "redirect:/liststudents";
	}

	@RequestMapping(value = { "/edit-{studentID}-student" }, method = RequestMethod.GET)
	public ModelAndView editStudent(@PathVariable int studentID, @ModelAttribute("student") Student persistenceStudent) {
		persistenceStudent = studentsService.getSpecificStudent(studentID);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("student", persistenceStudent);

		List<Classroom> classrooms = classroomService.getAllClassStudent();
		model.put("classrooms", classrooms);

		Map<String, Map<String, Object>> modelForView = new HashMap<String, Map<String, Object>>();
		modelForView.put("vars", model);
		

		return new ModelAndView("editStudent", modelForView);
	}

	@RequestMapping(value = { "/edit-{studentID}-student" }, method = RequestMethod.POST)
	public String updateStudent(@ModelAttribute("student") Student student, @PathVariable int studentID, Model model) {
		Student persistenceStudent = studentsService.getSpecificStudent(studentID);
		Classroom persistenceClassroom = classroomService.getSpecificClassroom(student.getClassroom().getName());
		System.out.println("Student id" + studentID);
		System.out.println("Student Id" + student.getId());
		System.out.println("Student Name" + student.getName());
		System.out.println("student address" + student.getAddress());
		System.out.println("Student class" + student.getClassroom());
		System.out.println("Student class name " + student.getClassroom().getName());
		System.out.println("Student class name " + student.getClassroom().getId());
		
		persistenceStudent.setAddress(student.getAddress());
		persistenceStudent.setClassroom(persistenceClassroom);
		persistenceStudent.setName(student.getName());
		
		Photo p = new Photo();
		p.setPhotolocation("hsjgjsh");
		p.setPhotoName("hagshdgashgf");
		
		persistenceStudent.setPhoto(p);
		
		studentsService.updateStudent(persistenceStudent);
		return "redirect:/liststudents";
	}

}
