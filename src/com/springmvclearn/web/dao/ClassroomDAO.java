package com.springmvclearn.web.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component("classroomDao")
public class ClassroomDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public void saveClassroom(Classroom classroom) {
		Transaction trans = session().beginTransaction();
		session().persist(classroom);
		trans.commit();
	}

	@SuppressWarnings("unchecked")
	public List<Classroom> getAllClassrooms() {

		Transaction trans = session().beginTransaction();
		List<Classroom> classes = session().createQuery("from Classroom").list();
		trans.commit();
		return classes;
	}

	public Classroom getSpecificClassroom(int classroomID) {
		Transaction trans = session().beginTransaction();
		Classroom classroom = (Classroom) session().get(Classroom.class, classroomID);
		trans.commit();
		return classroom;
	}

	public void deleteClassroomById(String classroomID) {
		Transaction trans = session().beginTransaction();
		Query query = session().createQuery("delete from Classroom where id = :classroomID");
		query.setString("classroomID", classroomID);
		query.executeUpdate();
		trans.commit();
	}

	public void updateClassroom(Classroom classroom) {
		Transaction trans = session().beginTransaction();
		session().saveOrUpdate(classroom);
		trans.commit();
	}

	public Classroom getSpecificClassroom(String classroomName) {
		Transaction trans = session().beginTransaction();
		String hql = "from Classroom c where c.name= :cname";
		Query q = session().createQuery(hql);
		q = q.setString("cname", classroomName);
		Classroom classroom = (Classroom) q.uniqueResult();
		trans.commit();
		return classroom;
	}

}
