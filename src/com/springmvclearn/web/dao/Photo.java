package com.springmvclearn.web.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "photo")
public class Photo {
	
	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;
	
	@Column(name="photo_name")
	private String photoName;
	
	@Column(name="photo_location")
	private String photolocation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhotoName() {
		return photoName;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	public String getPhotolocation() {
		return photolocation;
	}

	public void setPhotolocation(String photolocation) {
		this.photolocation = photolocation;
	}
	
	

}
