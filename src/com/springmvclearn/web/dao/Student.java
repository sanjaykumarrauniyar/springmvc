package com.springmvclearn.web.dao;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Student")
public class Student {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "address")
	private String address;

	@ManyToOne
	//@JoinColumn(name = "classid", nullable = false) //here column name can be changed as per requirement
	private Classroom classroom;
	
	
	@OneToOne(cascade=CascadeType.ALL)
	private Photo photo;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	


	
	
	

}
