package com.springmvclearn.web.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component("studentDao")
public class StudentDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveStudent(Student student){
		Transaction trans = session().beginTransaction();
		session().saveOrUpdate(student);
		trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<Student> getAllStudent(){
		Transaction trans = session().beginTransaction();
		List<Student> student = session().createQuery("from Student").list();
		trans.commit();
		return student;
	}
	

	public Student getSpecificStudent(int studentID) {
		Transaction trans = session().beginTransaction();
		Student student = (Student) session().get(Student.class, studentID);
		trans.commit();
		return student;
	}

	public void deleteStudentById(String studentID) {
		Transaction trans = session().beginTransaction();
		Query query = session().createQuery("delete from Student where id = :studentID");
		query.setString("studentID", studentID);
		query.executeUpdate();
		trans.commit();
	}

	public void updateStudent(Student student) {
		Transaction trans = session().beginTransaction();
		session().saveOrUpdate(student);
		trans.commit();
	}

}
