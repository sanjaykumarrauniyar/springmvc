package com.springmvclearn.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.ClassroomDAO;
import com.springmvclearn.web.dao.Student;

@Service("classservice")

public class ClassroomService {
	@Autowired
	private ClassroomDAO classdao;

	public void setClassDao(ClassroomDAO classesdao) {
		this.classdao = classesdao;
	}

	public List<Classroom> getAllClassStudent() {

		return classdao.getAllClassrooms();
	}

	public void saveClassroom(Classroom classes) {
		classdao.saveClassroom(classes);
	}

	public Classroom getSpecificClassroom(int classStudentID) {
		return classdao.getSpecificClassroom(classStudentID);
	}

	public void deleteClassroomById(String classStudentID) {
		classdao.deleteClassroomById(classStudentID);
	}

	public void updateClassroom(Classroom classStudent, int classStudentID) {
		Classroom persistClassStudent = classdao.getSpecificClassroom(classStudentID);
		persistClassStudent.setName(classStudent.getName());
		persistClassStudent.setRoomNo(classStudent.getRoomNo());
		classdao.updateClassroom(persistClassStudent);
	}
	
	
	public Classroom getSpecificClassroom(String classroomName) {
		return classdao.getSpecificClassroom(classroomName);
	}

	

}
