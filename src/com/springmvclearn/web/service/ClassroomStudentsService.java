package com.springmvclearn.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.ClassroomDAO;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.dao.StudentDAO;

@Service("classstudentsservice")
public class ClassroomStudentsService {

	@Autowired
	private ClassroomDAO classroomDAO;

	@Autowired
	private StudentDAO studentDAO;

	public void saveStudentToClassroom(String classroomName, List<Student> students) {
		Classroom persistClassroom = classroomDAO.getSpecificClassroom(classroomName);
		for (Student student : students) {
			student.setClassroom(persistClassroom);
			studentDAO.saveStudent(student);
		}

	}

}
