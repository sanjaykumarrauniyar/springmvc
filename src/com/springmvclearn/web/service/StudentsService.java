package com.springmvclearn.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.dao.StudentDAO;

@Service
public class StudentsService {

	@Autowired
	private StudentDAO studentDAO;
	
	public void saveStudent(Student student) {
		studentDAO.saveStudent(student);
	}
	
	public List<Student> getAllStudent() {

		return studentDAO.getAllStudent();
	}

	public Student getSpecificStudent(int studentID) {
		return studentDAO.getSpecificStudent(studentID);
	}

	public void deleteStudentById(String studentID) {
		studentDAO.deleteStudentById(studentID);
	}
	
	
	public void updateStudent(Student student){
		studentDAO.updateStudent(student);
	}
}
