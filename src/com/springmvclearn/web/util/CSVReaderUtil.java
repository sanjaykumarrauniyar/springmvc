package com.springmvclearn.web.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.springmvclearn.web.dao.Student;

public class CSVReaderUtil {
	
	private static final String csvfile = "E:/Test/student.csv";
	
	public static List<Student> readStudentCSVFile() {
		List<Student> studentdata = new ArrayList<Student>();
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvfile));

			while ((line = br.readLine()) != null) {
				String[] studentcsv = line.split(csvSplitBy);
				Student student = new Student();
				student.setName(studentcsv[0]);
				student.setAddress(studentcsv[1]);
				studentdata.add(student);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			File file = new File(csvfile);
			file.delete();
		}
		return studentdata;

	}
}
