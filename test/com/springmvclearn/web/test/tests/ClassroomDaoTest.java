package com.springmvclearn.web.test.tests;

import static org.junit.Assert.assertNotNull;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.springmvclearn.web.dao.Classroom;
import com.springmvclearn.web.dao.ClassroomDAO;
import com.springmvclearn.web.dao.Student;
import com.springmvclearn.web.dao.StudentDAO;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/springmvclearn/web/config/dao-context.xml",
		"classpath:com/springmvclearn/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class ClassroomDaoTest {

	@Autowired
	private ClassroomDAO classroomDAO;

	@Autowired
	private StudentDAO studentDAO;

	// @Test
	public void saveClassStudent() {

		List<Student> students = new ArrayList<>();

		Classroom classroom = new Classroom();
		classroom.setName("One");
		classroom.setStudents(students);

		Student studentA = new Student();
		studentA.setAddress("KTM");
		studentA.setClassroom(classroom);
		studentA.setName("ABC");

		students.add(studentA);

		classroomDAO.saveClassroom(classroom);

	}

	// @Test
	public void getClassroom() {
		String classroomName = "one";
		Classroom classroom = classroomDAO.getSpecificClassroom(classroomName);
		assertNotNull(classroom);

	}

	@Test
	public void saveStudentToClassroom() {
		String classroomName = "one";
		Classroom classroom = classroomDAO.getSpecificClassroom(classroomName);

		assertNotNull(classroom);

		Student studentA = new Student();
		studentA.setAddress("KTM");
		studentA.setClassroom(classroom);
		studentA.setName("ABC");

		studentDAO.saveStudent(studentA);
		List<Student> studentlist = studentDAO.getAllStudent();

		assertEquals(studentlist.size(), 1);
	}

}
